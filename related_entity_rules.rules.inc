<?php

/**
 * @return array
 *
 * Implements hook_rules_action_info().
 */
function related_entity_rules_rules_action_info() {
  return array(
    'fetch_referenced_nodes' => array(
      'label' => t('Fetch list of referenced nodes'),
      'group' => t('Entities'),
      'parameter' => array(
        'node' => array(
          'type' => 'entity',
          'label' => t('Referencing node'),
          'description' => t('The referencing node (usually an updated node.'),
          'default' => 'node',
        ),
        // TODO : use list or use entity data selector and then format value to get field machine name?
        'entity_reference_field' => array(
          'type' => 'text',
          'label' => t('Entity reference field'),
          'description' => t('The machine name of the entity reference field to retrieve the entities from.'),
          'restriction' => 'input',
        ),
      ),
      'provides' => array(
        'referenced_entity_id_list' => array(
          'type' => 'list<integer>',
          'label' => t('A list of referenced entity ids'),
        ),
      ),
    ),
    'fetch_referencing_nodes' => array(
      'label' => t('Fetch list of referencing nodes'),
      'group' => t('Entities'),
      'parameter' => array(
        'node' => array(
          'type' => 'entity',
          'label' => t('Node being referenced'),
          'description' => t('The referencing node (usually an updated node.'),
          'default' => 'node',
        ),
        'referencing_node type' => array(
          'type' => 'text',
          'label' => t('Referencing node type'),
          'description' => t('Node type that is referencing our node.'),
          'options list' => 'related_entity_rules_node_type_list',
          'restriction' => 'input',
        ),
        // TODO : use list or use entity data selector and then format value to get field machine name?
        'entity_reference_field' => array(
          'type' => 'text',
          'label' => t('Entity reference field'),
          'description' => t('The machine name of the entity reference field to retrieve the entities from.'),
          'restriction' => 'input',
        ),
      ),
      'provides' => array(
        'referencing_entity_id_list' => array(
          'type' => 'list<integer>',
          'label' => t('A list of referencing entity ids'),
        ),
      ),
    ),
  );
}

/**
 * @return array
 *
 * Helper function that provides a list of all node types.
 */
function related_entity_rules_node_type_list () {
  $node_types = node_type_get_types();
  $type_list = array();
  foreach ($node_types as $machine => $type) {
    $type_list[$machine] = $type->name;
  }
  return $type_list;
}

/**
 * @param $rule_state
 * @param $ref_field
 * @param $node
 * @return array
 *
 * Fetch list of referenced nodes.
 */
function fetch_referenced_nodes ($rule_state, $ref_field, $node) {
  // TODO: need to test this with other possible events
  $node_wrapped = $rule_state;
  return array('referenced_entity_id_list' => $node_wrapped->$ref_field->raw());
}

/**
 * @param $rule_state
 * @param $ref_node_type
 * @param $ref_field
 * @param $node
 * @return array
 *
 * Fetch list of referencing nodes.
 */
function fetch_referencing_nodes ($rule_state, $ref_node_type, $ref_field, $node) {
  // TODO: need to test this with other possible events
  $node_wrapped = $rule_state;
  return array('referencing_entity_id_list' => get_multiple_referencing_entities($ref_field, $ref_node_type, $node_wrapped->nid->raw()));
}

/**
 * @param $entity_reference_field
 * @param $bundle
 * @param $target_id
 * @return bool|mixed
 *
 * Helper function to retrieve entities that reference the target id through an
 * entity reference field.
 */
function get_multiple_referencing_entities($entity_reference_field, $bundle, $target_id){
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', $bundle)
    ->fieldCondition($entity_reference_field, 'target_id', $target_id)
    ->range(0, 100);
  $result = $query->execute();

  // Return entity id's.
  if (isset($result['node'])) {
    return array_keys($result['node']);
  }
  else{
    return FALSE;
  }
}